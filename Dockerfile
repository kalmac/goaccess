FROM gregyankovoy/goaccess

RUN apk add --update --no-cache gzip

COPY goaccess.sh /usr/local/bin/

WORKDIR /opt/log
